import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class BufferReadWriteExample {

	public static void main(String[] args) throws IOException {
		readWriteCharByChar();
		readWriteLineByLine();
	}
	
	/**
	 * le de um ficheiro linha a linha e escreve-o noutro ficheiro
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void readWriteLineByLine() throws FileNotFoundException, IOException {
		BufferedReader in = new BufferedReader(new FileReader("ficheiroIn.txt"));
		BufferedWriter out = new BufferedWriter(new FileWriter("ficheiroOut_bufferedWriter_byline.txt"));
		String s;
		int k = 0;
		
		while( (s = in.readLine()) != null) {
			k++;
			out.write(s);
			System.out.println(s);

		}
		
		in.close();
		out.close();
		System.out.println("Foram copiadas " + k + " linhas.");
	}

	/**
	 * le de um ficheiro carater a carater e escreve-o noutro ficheiro
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void readWriteCharByChar() throws FileNotFoundException, IOException {
		BufferedReader in = new BufferedReader(new FileReader("ficheiroIn.txt"));
		BufferedWriter out = new BufferedWriter(new FileWriter("ficheiroOut_bufferedWriter.txt"));
		int n, k = 0;
		
		while( (n = in.read()) != -1) {
			k++;
			out.write(n);
			System.out.println(n + " / " + (char) n );
		}
		
		in.close();
		out.close();
		System.out.println("Foram copiados " + k + " carateres.");
	}

}
