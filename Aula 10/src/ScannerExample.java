import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerExample {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner scan = new Scanner(new File("ficheiroIn.txt"));
		int k = 0;
		
		while(scan.hasNextLine()) {
			String s = scan.nextLine();
			k++;
			System.out.println(s);
		}
		
		scan.close();
		System.out.println("Foram copiadas " + k + " linhas.");
	}

}
