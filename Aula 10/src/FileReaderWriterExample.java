import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderWriterExample {

	/**
	 * le de um ficheiro carater a carater e escreve-o noutro ficheiro
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		FileReader in = new FileReader("ficheiroIn.txt");
		FileWriter out = new FileWriter("ficheiroOut_filewriter.txt");
		
		int k = 0, n, nLinhas = 0;;

		while( (n = in.read()) != -1 ) {
			k++;
			out.write(n);
			System.out.println(n + " / " + (char) n );
			
			if ((char) n == '\n') {
				nLinhas ++;
			}
		}
		
		in.close();
		out.close();
		
		System.out.println("Foram copiados " + k + " carateres. E " + nLinhas + " linhas (!)");
	}

}
