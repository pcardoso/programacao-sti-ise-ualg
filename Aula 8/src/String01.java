
public class String01 {
	public static void main(String[] args) {
		referencias();

		declaracao();

		acessoCarateres();
		
		operacoesComStrings();
		
		igualdadeDeStrings();
		

		
		
	}

	public static void igualdadeDeStrings() {
		String r = new String("abc");
		String s = new String("abc");
		String t = s;
		
		System.out.println("System.identityHashCode(r): " + System.identityHashCode(r));
		System.out.println("System.identityHashCode(s): " + System.identityHashCode(s));
		System.out.println("System.identityHashCode(t): " + System.identityHashCode(t));
		
		System.out.println("r == s -> " + (r == s));
		System.out.println("r.equals(s) -> " + r.equals(s));

		
		
		System.out.println("t == s -> " + (t == s));
		System.out.println("t.equals(s) -> " + t.equals(s));
	}

	public static void operacoesComStrings() {
		String s = "STI rules!";
		System.out.println(s.length());
		
		System.out.println(s.charAt(1));
		
		System.out.println(s.substring(4, 9));
		
		System.out.println(s.indexOf("rul"));
		
		System.out.println(s.startsWith("STI"));
		System.out.println(s.endsWith("STI"));

		String t = " They are the best!";
		s = s.concat(t);
		System.out.println(s);
		
		System.out.println(s.replace("S", "s"));
		
		for(String a : s.split(" ")) {
			System.out.println(a);
		}
		System.out.println();
	}

	public static void acessoCarateres() {
		String s = "abcdefghi...";
		for(int i = 0; i < s.length(); i++) {
			System.out.println(s.charAt(i));
		}
		
		char[] c = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', '.', '.', '.'};
		for(int i = 0; i < c.length; i++) {
			System.out.println(c[i]);
		}
	}

	public static void referencias() {
		String name1 = "Ada, Countess of Lovelace";
		String name2 = "Grace Murray Hopper";

		System.out.println(System.identityHashCode(name1) + ": " + name1);
		System.out.println(System.identityHashCode(name2) + ": " + name2);

		name1 = "Grace Murray Hopper";
		System.out.println(System.identityHashCode(name1) + ": " + name1);		

		name1 = name2;
		System.out.println(System.identityHashCode(name1) + ": " + name1);	
	}



	public static void declaracao() {
		// declaração e instanciação curta
		String s1 = "abc";
		// declaração e instanciação "longa"
		String s2 = new String("abc");

		System.out.println(System.identityHashCode(s1) + ": " + s1);
		System.out.println(System.identityHashCode(s2) + ": " + s2);
	}
}
