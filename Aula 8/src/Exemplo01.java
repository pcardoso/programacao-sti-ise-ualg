
public class Exemplo01 {

	public static class StringMutation
	{
		public static void main(String[] args)
		{
			mutateString();

			reversePalindrome();

			findInGenome();

		}

		public static void findInGenome() {
			String start = "ATG";
			String stop  = "TAG";
			String genome = "ATAGATGCATAGCGCATAGCTAGATGTGCTAGCAT";


			printGenome(genome);

			int startIdx = genome.indexOf(start);

			while(startIdx != -1) {
				genome = genome.substring(startIdx);
				printGenome(genome);

				int stopIdx = genome.indexOf(stop);
				while(stopIdx != -1) {
					if( stopIdx >= 6 ) {
						System.out.println(genome.substring(3, stopIdx));
					}
					stopIdx = genome.indexOf(stop, stopIdx + 1);
				}


				if (genome.length() > 3) {
					genome = genome.substring(3);
					startIdx = genome.indexOf(start);	
				}else{
					break;
				}
			}
		}

		public static void printGenome(String genome) {
			System.out.println("genome = '" + genome + "'");
		}

		

		public static void reversePalindrome() {
			String palindrome = "Dot saw I was Tod";

			int len = palindrome.length();

			char[] CharArray = new char[len]; //n�o � uma string � um conj. de char

			for (int i = 0; i < len; i++) 
				CharArray[len - 1 - i] = palindrome.charAt(i); //conj. de caracteres

			String reversePalindrome = new String(CharArray); //string (�objecto)

			System.out.println(reversePalindrome);
		}

		public static void mutateString() {
			String phrase = "Change is inevitable";
			String mutation1, mutation2, mutation3, mutation4;

			System.out.println("Original string: \"" + phrase + "\"");
			System.out.println("Length of string: " + phrase.length());

			mutation1 = phrase.concat(", except from vending machines.");
			mutation2 = mutation1.toUpperCase();
			mutation3 = mutation2.replace('E', 'X');
			mutation4 = mutation3.substring(3, 30);

			// Print each mutated string
			System.out.println("Mutation #1: " + mutation1);
			System.out.println("Mutation #2: " + mutation2);
			System.out.println("Mutation #3: " + mutation3);
			System.out.println("Mutation #4: " + mutation4);
			System.out.println("Mutation #4 length: " + mutation4.length());
		}
	}

}
