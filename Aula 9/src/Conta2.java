
public class Conta2 {
	private String dono;

	private double limite;

	private int numero;

	private double saldo;

	public void deposita(double valor) {
		this.saldo += valor;
	}

	public String getDono() {
		return dono;
	}

	public double getLimite() {
		return limite;
	}

	public int getNumero() {
		return numero;
	}

	public double getSaldo() {
		return saldo;
	}
	
	public boolean saca(double valor) {
		if(valor > saldo) 
			return false;
		this.saldo -= valor;
		return true;
	}
	
	public void setDono(String dono) {
		this.dono = dono;
	}
	
	public void setLimite(double limite) {
		this.limite = limite;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	
	public boolean transfere(Conta2 outraConta, double valor) {
		if(this.saca(valor)) {
			outraConta.deposita(valor);
			return true;
		}
		return false;
		
	
	}
	
	

}
