
public class Programa2 {

	public static void main(String[] args) {
		Conta2 minhaConta = new Conta2();
		
		minhaConta.setDono("Duke");
		minhaConta.setSaldo(1000.0);
		
		System.out.println("done: " + minhaConta.getDono() + "\tsaldo: " + minhaConta.getSaldo());
		boolean consegui = minhaConta.saca(2000);
		if (consegui) {
			System.out.println("Consegui sacar");
		} else {
			System.out.println("N�o consegui sacar");
		}
		
		System.out.println("done: " + minhaConta.getDono() + "\tsaldo: " + minhaConta.getSaldo());
		minhaConta.deposita(500);
		System.out.println("done: " + minhaConta.getDono() + "\tsaldo: " + minhaConta.getSaldo());
		
		
		// 
		Conta2 meuSonho = new Conta2();
		meuSonho.setDono("Dreamer");
		meuSonho.setSaldo(15000000);
		System.out.println("done: " + meuSonho.getDono() + "\tsaldo: " + meuSonho.getSaldo());
		
		
		meuSonho.transfere(minhaConta, 500000);
		System.out.println("done: " + minhaConta.getDono() + "\tsaldo: " + minhaConta.getSaldo());
		System.out.println("done: " + meuSonho.getDono() + "\tsaldo: " + meuSonho.getSaldo());		
		
		
	}

}
