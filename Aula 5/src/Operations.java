
public class Operations {

	public static void main(String[] args) {
		int N = 10;
		double[] myArray = new double[N];

		// cria array de valores aleatórios
		for(int i = 0; i < N; i++) {
			myArray[i] = Math.random();
		}
		
		// print values
		for(double v: myArray) {
			System.out.print(v + " ");
		}
		System.out.println("");
		
		// find maximum
		double max = Double.NEGATIVE_INFINITY;
		for(int i = 0; i < N; i++) {
			if(myArray[i] > max)
				max = myArray[i];
		}
		System.out.println("Max = " + max);
		
		// average
		double sum = 0;
		for(double v: myArray) {
			sum += v;
		}
		System.out.println("average = " + sum / N);
		
		// copy
		double[] copy = new double[N];
		for(int i = 0; i < N; i++) {
			copy[i] = myArray[i];
		}
		
		//reverse
		for(int i = 0; i < N / 2.; i++) {
			double t = myArray[i];
			myArray[i] = myArray[N - i - 1];
			myArray[N - i - 1] = t;
		}
		
		// print values
		for(double v: myArray) {
			System.out.print(v + " ");
		}
		System.out.println("");

	}

}
