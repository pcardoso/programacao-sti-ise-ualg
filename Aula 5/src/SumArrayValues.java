
public class SumArrayValues {

	public static void main(String[] args) {
		double[] myArray = {1, 5, 23, 114, 15}; //new double[5];
		System.out.println("----------------");
		for(double v: myArray) {
			System.out.println(v);
		}
		
		System.out.println("----------------");
		for(int i = 0; i < 5; i++) {
			System.out.println(myArray[i]);			
		}
		
		System.out.println("----------------");
		double sum = 0;
		for(double v: myArray) {
			sum += v;
		}
		System.out.println("soma = " + sum);
		
		System.out.println("----------------");
		sum = 0;
		for(int i = 0; i < 5; i++) {
			sum += myArray[i];
		}
		System.out.println("soma = " + sum);
		
		
	}

}
