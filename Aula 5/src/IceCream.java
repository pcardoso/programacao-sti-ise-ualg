public class IceCream
{
	enum Flavor {vanilla, chocolate, strawberry, fudgeRipple, coffee, rockyRoad, mintChocolateChip, cookieDough}

	//-----------------------------------------------------------------
	// Creates and uses variables of the Flavor type.
	//-----------------------------------------------------------------
	
	public static void main(String[] args)
	{
		Flavor cone1, cone2, cone3;
		cone1 = Flavor.rockyRoad;
		cone2 = Flavor.chocolate;
		cone3 = Flavor.cookieDough;


		System.out.println("cone1 value: " + cone1);
		System.out.println("cone1 ordinal: " + cone1.ordinal());
		System.out.println("cone1 name: " + cone1.name());
		System.out.println();
		System.out.println("cone2 value: " + cone2);
		System.out.println("cone2 ordinal: " + cone2.ordinal());
		System.out.println("cone2 name: " + cone2.name());
		System.out.println();
		System.out.println("cone3 value: " + cone3);
		System.out.println("cone3 ordinal: " + cone3.ordinal());
		System.out.println("cone3 name: " + cone3.name());
		
		System.out.println("------------------------------");
		// podemos tamb�m iterar sobre todos os valores...
		for(Flavor f: Flavor.values()) {
			System.out.println("value: " + f);
			System.out.println("ordinal: " + f.ordinal());
			System.out.println("name: " + f.name());
			
		}
	}
}