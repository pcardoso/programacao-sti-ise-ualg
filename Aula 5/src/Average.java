
public class Average {

	public static void main(String[] args) {
		double[][] a = {{99, 85, 98}, {98, 57, 78}, {92, 77, 76}, {92, 34, 11}};
		
		int M = a.length; // numero de linhas
		int N = a[0].length; // numero de columas
		
		
		double[] mediaPorLine = new double[M];
		double[] mediaPorColuna = new double[N];
		
		// calcula media por linha
		for(int lin = 0; lin < M; lin++) {
			double sum = 0;
			for(int col = 0; col < N; col++) {
				sum += a[lin][col];
			}
			mediaPorLine[lin] = sum / N;
		}
		
		// calcula media por coluna
		for(int col = 0; col < N; col++) {
			double sum = 0;
			for(int lin = 0; lin < M; lin++) {
				sum += a[lin][col];
			}
			mediaPorColuna[col] = sum / M;
		}
		
		// printing...
		
		for(int lin = 0; lin < M; lin++){
			for (int col = 0; col < N; col++) {
				System.out.print(a[lin][col] + "\t");
			}
			System.out.printf("| %.2f\n", mediaPorLine[lin]);
		}
		System.out.println("------------------------");
		for(int col = 0; col < N; col++) {
			System.out.printf("%.2f\t", mediaPorColuna[col]);
		}

		
		
	}

}
