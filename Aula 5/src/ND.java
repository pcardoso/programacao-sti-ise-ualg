
public class ND {

	public static void main(String[] args) {

		// 2D
		int N = 3;
		int[][] a = new int[N][N];

		for(int i = 0; i < N; i++) {
			for(int j = 0; j < N; j++) {
				a[i][j] = i * N + j;
			}
		}

		for(int i = 0; i < N; i++) {
			for(int j = 0; j < N; j++) {
				System.out.print(a[i][j]+ " ");
			}
			System.out.println();
		}


		// another way of doing things
		System.out.println("-------");
		int[][] b = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

		for(int i = 0; i < N; i++) {
			for(int j = 0; j < N; j++) {
				System.out.print(b[i][j]+ " ");
			}
			System.out.println();
		}



		// 3 D
		System.out.println("-------");

		int [][][] c = new int[N][N][N];

		for(int i = 0; i < N; i++) {
			for(int j = 0; j < N; j++) {
				for(int k = 0; k < N; k++) {
					c[i][j][k] = i * j * k;
				}
			}
		}

		
		int sum = 0;
		for(int i = 0; i < N; i++) {
			for(int j = 0; j < N; j++) {
				for(int k = 0; k < N; k++) {
					sum += c[i][j][k];
				}
			}
		}
		System.out.println("sum = " + sum);

		

	}

}
