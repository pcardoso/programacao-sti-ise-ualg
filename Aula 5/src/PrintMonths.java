
public class PrintMonths {

	public static void main(String[] args) {
		String[] months2 = new String[12];
		months2[0] = "Jan";
		months2[1] = "Feb";
		// ...
		
		
		String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", 
				"Aug", "Sep", "Oct", "Nov", "Dec"};

		for (int i=1; i<=12; i++) {
			System.out.println(i + ": " + months[i-1]);
		}

	}

}
