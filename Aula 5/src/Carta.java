import java.util.Scanner;

public class Carta {
	public static void main(String[] args) {
		int N=5;
		int IdadeMaisVelho=18;
		String NomePessoaMaisVelha="Nenhuma pode tirar carta";
		String[] nome = new String [N];
		int[] idade = new int [N];

		Scanner scan = new Scanner(System.in);

		//ler os nomes e as idades
		for (int i = 0; i < N; i++){
			System.out.print("nome "+i+":");
			nome[i] = scan.next();
			System.out.print("idade "+i+":");
			idade[i] = scan.nextInt();
		}

		//Imprimir o nome da pessoa mais velha
		for (int i = 0; i < N; i++){
			if (idade[i] > IdadeMaisVelho){
				IdadeMaisVelho = idade[i];
				NomePessoaMaisVelha = nome[i];
			}
		}
		System.out.print("A pessoa mais velha que tirou a carta �: " + NomePessoaMaisVelha);
		
		scan.close();
	}
}