import java.util.Scanner;

public class CaseDiaSemana {

	private static Scanner entrada;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		entrada = new Scanner(System.in);

		while (true) {
			System.out.print("Digite o dia da semana: ");
			int DiaSemana = entrada.nextInt();

			switch (DiaSemana) {
				case 1: 
					System.out.println("Segunda-feira");
					break;
				case 2: 
					System.out.println("terca-feira");
					break;
				case 3: 
					System.out.println("quarta-feira");
					break;
				case 4: 
					System.out.println("quinta-feira");
					break;
				case 5: 
					System.out.println("sexta-feira");
					break;
				case 6:
					System.out.println("sabado-feira");
					break;
				case 7: 
					System.out.println("domingo-feira");
					break;
				default:
					System.out.println("Nao e dia da semana");
					System.exit(0);
			}
		}
	}
}
