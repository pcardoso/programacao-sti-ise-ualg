import java.util.Scanner;

class CaseDiasDoMes {
	public static void main(String[] args) {

		int month = 0;
		int year = 0;
		int numDays = 0;
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Indique o m�s:");
		month = s.nextInt();

		switch (month) {
			case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				numDays = 31;
				break;
			case 4: case 6: case 9: case 11:
				numDays = 30;
				break;
			case 2:
				System.out.println("de que ano?");
				year = s.nextInt();
				if (((year % 4 == 0) && 
						!(year % 100 == 0))
						|| (year % 400 == 0))
					numDays = 29;
				else
					numDays = 28;
				break;
			default:
				System.out.println("M�s inv�lido.");
				break;
		}

		System.out.println("N� de dias = " + numDays);
		s.close();
	}
}