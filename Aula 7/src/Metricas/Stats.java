package Metricas;

public class Stats {
	
	
	/** calcula a media de array
	 * @param array - lista de doubles
	 * @return media de array
	 */
	public static double media(double[] array) {
		
		double sum = 0;
		
		for(int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		
		return sum / array.length;
	}	
	
	/** calcula o maior valor em array
	 * @param array - lista de doubles
	 * @return valor m�ximo em array
	 */
	public static double max(double[] array) {
		
		double max = Double.NEGATIVE_INFINITY;
		
		for(int i = 0; i < array.length; i++) {
			if(max < array[i]) {
				max = array[i];
			}
		}
		return max;
	}
	
	/** calcula o menor valor em array
	 * @param array - lista de doubles
	 * @return valor m�nimo em array
	 */
	public static double min(double[] array) {
		
		double min = Double.POSITIVE_INFINITY;
		
		for(int i = 0; i < array.length; i++) {
			if(min > array[i]) {
				min = array[i];
			}
		}
		return min;
	}
	
	public static void main(String[] args) {
		double[] myArray = {1.0, 2.0, 3.0, 4.0};
		
		System.out.println("media = " + media(myArray));
		System.out.println("max = " + max(myArray));
		System.out.println("min = " + min(myArray));

	}

}
