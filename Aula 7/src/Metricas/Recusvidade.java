package Metricas;

public class Recusvidade {

	/**
	 * calcula n! = n x (n-1) x (n-2) x ... x 1 
	 * @param n: numero natural
	 * @return n!
	 */
	public static int fact(int n) {
		if (n == 1) return 1;
		return n * fact(n - 1);
	}
	
	/**
	 * [implementa��o recursiva] calcula o m�ximo divisor comum entre p e q. Ex.: mdc(15, 10) = 5
	 * @param p - n� natural
	 * @param q - n� natural
	 * @return mdc(p, q)
	 */
	public static int mdc_rec(int p, int q) {
		if (q == 0) return p;
		else return mdc_rec(q, p % q);
	}
	

	
	/**
	 * [implementa��o n�o recursiva] calcula o m�ximo divisor comum entre p e q. Ex.: mdc(15, 10) = 5
	 * @param p - n� natural
	 * @param q - n� natural
	 * @return mdc(p, q)
	 */
	public static int mdc_non_rec(int p, int q) {
		while( q != 0) {
			int temp = q;
			q = p % q;
			p = temp;
		}
		return p;
	}
	
	
	public static void main(String[] args) {
		System.out.println("10! = " + fact(10));

		System.out.println("mdc(10, 15) = " + mdc_non_rec(10,  15));
		System.out.println("mdc(10, 15) = " + mdc_rec(10,  15));
	}

}
