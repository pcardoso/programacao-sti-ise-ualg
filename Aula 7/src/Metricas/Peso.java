package Metricas;

public class Peso {

	public static void main(String[] args) {
		double[] pesos = {75, 83, 92};
		
		System.out.println("pessoa mais pesada: " + Stats.max(pesos));
		System.out.println("pessoa mais leve: " + Stats.min(pesos));
		System.out.println("M�dia de pesos: " + Stats.media(pesos));
	}

}
