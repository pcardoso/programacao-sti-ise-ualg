public class function3 {
	/**
	 * imprime os valores de a e b
	 * @param x : valor inteiro
	 * @param y : valor inteiro
	 */
	public static void imprime (int x, double y){
		System.out.println("a=" + x + " b=" + y);
	}

	/**
	 * soma x a num
	 * @param x : valor inteiro
	 * @param num : valor inteiro
	 * @return devolve a soma de x com num
	 */
	public static int SomaNum (int x, int num){
		x=x+num;
		return x;
	}

	/**
	 * soma x a num
	 * @param x : valor double
	 * @param num : valor double
	 * @return devolve a soma de x com num
	 */
	public static double SomaNum (double x, double num){
		x=x+num;
		return x;
	}

	/**
	 * main function 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a=10;
		double b=20.5;

		a = SomaNum (a, 1);
		b = SomaNum (b, 5.5);
		imprime (a, b);

		System.out.println("\n");
		a = SomaNum (a, 5);
		b = SomaNum (b, 7.2);
		imprime (a, b);
	}
}