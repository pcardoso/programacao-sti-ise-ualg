/**@author Jo�o
 * random generate number and compute how many times is necessary to fill all spaces of a 
 * deck of cards (52)
 */


public class Coupon {
	/******************************************************************************************
	 * Test the cards until fill all deck
	 * @param N the highest number to be generated
	 * @return return number of cards you collect before obtaining one of each of the N types
	 */
	public static int collect(int N) {
		boolean[] found = new boolean[N];     // found[i] = true if card type i already collected
		int cardcnt = 0;                      // number of cards collected
		int valcnt  = 0;                      // number of distinct card types collected

		//initialize found
		for(int i = 0; i < N; i++) {
			found[i] = false;
		}
		
		// repeat until you've collected all N card types
		while (valcnt < N) {
			int val = (int) (Math.random() * N);    // pick a random card 
		cardcnt++;                        			// one more card
			if (!found[val]) valcnt++;        		// discovered a new card type
			found[val] = true;             			// mark card type as having been collected
		}
		return cardcnt;
	}
	/*********************************************************************************************
	 * Program to compute the number of trials to fill a deck of cards 
	 * @param args argumento para a linha de comandos 
	 */
	public static void main(String[] args) {
		int N = 52;
		int count = collect(N);
		System.out.println(count);
	} 
} 
