import java.util.Scanner;

public class MoreExamples {
	/**
	 * m�dulo de um n� inteiro
	 * @param a
	 * @return
	 */
	public static int abs(int a) {
		return a >= 0? a: -a;
	}
	
	/**
	 * m�dulo de ou n� double
	 * @param a
	 * @return
	 */
	public static double abs(double a) {
		return a >= 0? a: -a;
	}
	
	/**
	 * verifica se N � n� primo
	 * @param N
	 * @return
	 */
	public static boolean isPrime(int N) {
		if (N < 2) return false;
		for (int i = 2; i < N / i; i++) // poss�vel de melhorar
			if(N % i == 0) return false;
		return true;
	}
	
	/**
	 * calcula hipotenusa de triangulo ret�ngulo com catetos a e b
	 * @param a
	 * @param b
	 * @return
	 */
	public static double hipotenusa(double a, double b) {
		return Math.sqrt(a * a + b * b);
	}
	
	/**
	 * compute the harmonic number
	 * @param N
	 * @return
	 */
	public static double H(int N) {
		double sum = 0.0;
		for(int i = 0; i <= N; i++)
			sum += 1.0 / i;
		return sum;
	}
	
	
	/**
	 * calcula o valor maximo de um vetor
	 * @param vetor: array de doubles
	 * @return valor m�ximo
	 */
	public static double max(double[] vetor) {
		double max = Double.NEGATIVE_INFINITY;
		for(double v : vetor)
			if (max < v) max = v;
		return max;
	}
	
	/**
	 * escreve a matriz A no stdout
	 * @param A : matriz 2D
	 */
	public static void print(double[][] A){
		for(int i = 0; i < A.length; i++) {
			for(int j = 0; j < A[0].length; j++) {
				System.out.print(A[i][j] + "\t");
			}
			System.out.println();	
		}
	}
	/**
	 * le uma matriz 2D do stdin
	 * @param M - numero de linhas
	 * @param N - numero de colunas
	 * @return matriz
	 */
	public static double[][] read2D(int M, int N){
		double[][] A = new double[M][N];
		Scanner s = new Scanner(System.in);
		
		for(int i = 0; i < M; i++) {
			for(int j = 0; j < N; j++) {
				System.out.printf("A[%d][%d] = ", i, j);
				A[i][j] = s.nextDouble();				
			}
		}
		s.close();
		return A;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		double[][] A = read2D(2, 2);
		
		print(A);
		
	}

}
