
public class ArrayAsArguments {
	/**
	 * imprime o vetor para o stdout
	 * @param vetor : array de doubles
	 */
	public static void print(double[] vetor) {
		for(double v: vetor)
			System.out.print(v + " ");
		System.out.println();
	}
	
	/**
	 * calcula a media de um vetor
	 * @param vetor: vetor de doubles
	 * @return media
	 */
	public static double mean(double[] vetor) {
		double sum = 0;
		for(double v: vetor) sum += v;
		return sum / vetor.length;
	}

	/**
	 * troca os valores nas posi��es i e j do vetor 
	 * @param vetor : vetor de valores
	 * @param i : posi��o i
	 * @param j : posi��o j
	 */
	public static void exch(double[] vetor, int i, int j) {
		double temp = vetor[i];
		vetor[i] = vetor[j];
		vetor[j] = temp;
	}

	public static void main(String[] args) {
		double[] vectorMain = {1, 2, 3, 4, 5};
		print(vectorMain);
		System.out.println("media = " + mean(vectorMain));
		
		exch(vectorMain, 1, 4);
		print(vectorMain);
	}



}
