
public class MultipleArgs_and_overload {

	public static double hypot(double x, double y) {
		return Math.sqrt(x * x + y * y);
	}
	
	public static int my_abs(int x) {
		System.out.println("chamada: int my_abs");
		if( x > 0 ) return x;
		return -x;
	}
	
	public static double my_abs(double x) {
		System.out.println("chamada: double my_abs");
		if( x > 0 ) return x;
		return -x;
	}
	
	public static void main(String[] args) {
		
		System.out.println("Hipotenusa do triangulo retangulo " + 
							"com catetos 3 e 4 � igual a " + hypot(3,  4));
		
		System.out.println(my_abs(-3));
		System.out.println(my_abs(-3.0));
		
	}

}
