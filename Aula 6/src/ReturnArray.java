
public class ReturnArray {

	/**
	 * imprime o vetor para o stdout
	 * @param vetor : array de doubles
	 */
	public static void print(double[] vetor) {
		for(double v: vetor)
			System.out.print(v + " ");
		System.out.println();
	}
	
	/**
	 * gera um vetor com os primeiros N multiplos de factor
	 * @param factor: valor para os quais s�o gerados os m�ltiplos
	 * @param N: n�mero de m�ltiplos
	 * @return vetor de m�ltiplos
	 */
	public static double[] FillArray(double factor, int N)
	{
		double a[] = new double[N+1];
	
		for (int i = 0; i <= N; i++)
			a[i] = i*factor;
	
		return a;
	}

	
	public static void main(String[] args) {
		double a[] = FillArray(5,  10);
		print(a);
	}

}
