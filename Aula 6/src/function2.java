public class function2 {
	/**
	 * imprime os valores de a e b
	 * @param x : valor inteiro
	 * @param y : valor inteiro
	 */
	public static void imprime (int x, int y){
		System.out.println("a=" + x + " b=" + y);
	}

	/**
	 * soma x a num
	 * @param x : valor inteiro
	 * @param num : valor inteiro
	 * @return devolve a soma de x com num
	 */
	public static int SomaNum (int x, int num){
		return x + num;
	}

	/**
	 * main function 
	 * @param args
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub
		int a=10;
		int b=20;

		a = SomaNum(a, 1); 
		b = SomaNum(b, 5);
		imprime (a, b);

		System.out.println("\n");

		a = SomaNum(a, 5);
		b = SomaNum(b, 7);
		imprime (a, b);
	}
}
