
public class Newton {

	public static double sqrt(double c) {
		if (c < 0) return Double.NaN;
		
		double err = 1e-15;
		double t = c;
		
		while(Math.abs(t-c/t) > err * t) {
			t = (c/t + t) / 2;
		}
		return t;
	}
	public static void main(String[] args) {
		int N = args.length;
		
		if( N == 0 ) { 
			System.out.println("Newton value_1  value_2  ... " );
			return ;
		}
		
		double[] arrayOfDoubles = new double[N];
		
		for(int i = 0; i < N; i++) {
			arrayOfDoubles[i] = Double.parseDouble(args[i]);
		}
		
		for(double x: arrayOfDoubles) {
			System.out.printf("sqrt(%f) = %f\n", x, sqrt(x)); 
		}

	}

}
