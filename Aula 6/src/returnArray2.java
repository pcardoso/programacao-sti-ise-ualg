public class returnArray2 {
	//----------------------OVERLOAD--------------------------
	public static double[] FillVector(int N) {
		double a[] = new double[N+1];

		for (int i = 0; i <= N; i++)
			a[i] = -1;
		return a;  
	}
	//-------------------OVERLOAD------------------------------------------
	public static double[] FillVector(int N, double value) {
		double a[] = new double[N+1];

		for (int i = 0; i <= N; i++)
			a[i] = value;
		return a;  
	}
	//-------------------------------------------------------------     
	public static void main(String[] args) {
		int i;
		int N = 5;
		double value = 1;
		double array1[] = new double[N + 1];
		double array2[] = new double[N + 1];

		array1 = FillVector(N);
		array2 = FillVector(N, value);

		for (i=0; i<=N; i++){
			System.out.println("A[" + i + "]: " + array1[i] + "  " + array2[i]);
		}//for
	}//main
}//class
